# Heat and Thermodynamics

---

This is a compilation of solutions, only the solutions, to the problems found
in *Heat and Thermodynamics (7th ed.)* by Mark W. Zemansky and Richard H. Dittman.

[[_TOC_]]

## Temperature and the Zeroth Law of Thermodynamics

1. [Problem 1](./solutions/p0101.md)
2. [Problem 2](./solutions/p0102.md)
3. [Problem 3](./solutions/p0103.md)
9. [Problem 9](./solutions/p0109.md)

## Simple Thermodynamic Systems

## Work

6. [Problem 6](./solutions/p0406.md)

## Heat and the First Law of Thermodynamics
## Ideal Gas
## The Second Law of the Thermodynamics
## The Carnot Cycle and the Thermodynamic Temperature Scale
## Entropy
## Pure Substances
## Mathematical Methods
## Open Systems
## Statistical Mechanics
## Thermal Properties of Solids
## Critical Phenomena; Higher-Order Phase Transitions
## Chemical Equilibrium
## Ideal-Gas Reactions
## Heterogenous Systems
