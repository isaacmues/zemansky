# Problem 9

The conversion from Celsius to Farenheit is given by

$$
\begin{align}
    \theta(^\circ\mathrm{F}) &= \frac{9}{5}(^\circ\mathrm{C}) + 32 \\
                             &= \frac{9}{5}(99.974) + 32 \\
                             &= 211.95^\circ\ \mathrm{F}.
\end{align}
$$
